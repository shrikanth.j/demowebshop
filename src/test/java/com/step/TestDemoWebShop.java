package com.step;

import java.time.Duration;

import org.asynchttpclient.util.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class TestDemoWebShop {
	
	WebDriver driver;
	
	@Given("Launch the Application")
	public void launch_the_application() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	    driver.get("https://demowebshop.tricentis.com/");
	}

	@Given("Login to the application")
	public void login_to_the_application() {
	    driver.findElement(By.linkText("Log in")).click();
	}

	@When("give the username {string} and password {string}")
	public void give_the_username_and_password(String string, String string2) {
		driver.findElement(By.id("Email")).sendKeys(string);
		driver.findElement(By.id("Password")).sendKeys(string2);
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@Given("Select the Books category")
	public void select_the_books_category() {
	    driver.findElement(By.linkText("Books")).click();
	}

	@When("Sorted the High to Low price")
	public void sorted_the_high_to_low_price() {
	    WebElement sortBy= driver.findElement(By.id("products-orderby"));
	    
	    Select sel = new Select(sortBy);
	    sel.selectByVisibleText("Price: High to Low");
	}

	@When("Select and add two books in to Add to Cart")
	public void select_and_add_two_books_in_to_add_to_cart() throws InterruptedException {
		driver.findElement(By.xpath("(//input[@type=\"button\"])[3]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//input[@type=\"button\"])[5]")).click();
		Thread.sleep(5000);

	}

	@Given("Select Electronics category")
	public void select_electronics_category() {
	    driver.findElement(By.linkText("Electronics")).click();
	}

	@When("Select Cell phones")
	public void select_cell_phones() {
	    driver.findElement(By.linkText("Cell phones")).click();
	}

	@When("add that phone in to Add to Cart")
	public void add_that_phone_in_to_add_to_cart() {
	    driver.findElement(By.xpath("(//img[@alt='Picture of Smartphone'])[2]")).click();
	    driver.findElement(By.xpath("//input[@class='button-1 add-to-cart-button']")).click();
	}

	@Then("Display the count of items added to the cart")
	public void display_the_count_of_items_added_to_the_cart() {
	   String count = driver.findElement(By.xpath("//a[@class='ico-cart']//span[2]")).getText();
	   System.out.println("Number of added items = "+count);
	}

	@Given("Select Gift Cards category")
	public void select_gift_cards_category() {
	    driver.findElement(By.linkText("Gift Cards")).click();
	}

	@Then("It should shows four item per page")
	public void it_should_shows_four_item_per_page() {
	  WebElement display = driver.findElement(By.id("products-pagesize"));
	  
	  Select sel1 = new Select(display);
	  sel1.selectByVisibleText("4");
	}

	@Then("Capture the name and price of that gift cards")
	public void capture_the_name_and_price_of_that_gift_cards() {
	    driver.findElement(By.xpath("//img[@alt='Picture of $25 Virtual Gift Card']")).click();
	    String giftCardName = driver.findElement(By.xpath("//h1[@itemprop='name']")).getText();
	    String giftCardPrice = driver.findElement(By.xpath("//span[@itemprop='price']")).getText();
	    
	    System.out.println("Name of the Gift Card = "+giftCardName);
	    System.out.println("Price of the Gift Card = "+giftCardPrice);
	}

	@Given("Logout the application")
	public void logout_the_application() {
	    driver.findElement(By.linkText("Log out")).click();
	}

	@Then("Check the login link is displayed or not")
	public void check_the_login_link_is_displayed_or_not() {
	    String actual = driver.findElement(By.linkText("Log in")).getText();
	    String expected = "Log in";
	    Assert.assertEquals(expected, actual);
	    
	    driver.quit();
	}

	
}
