Feature: Testing the DemoWebShop Application
#Author Shrikanth S J

Scenario: end to end testing of the DemoWebShop Application
Given Launch the Application
Given Login to the application
When give the username "manzmehadi1@gmail.com" and password "Mehek@110"
Given Select the Books category
When Sorted the High to Low price
And Select and add two books in to Add to Cart
Given Select Electronics category
When Select Cell phones
And add that phone in to Add to Cart
Then Display the count of items added to the cart
Given Select Gift Cards category 
Then It should shows four item per page
And Capture the name and price of that gift cards
Given Logout the application
Then Check the login link is displayed or not

